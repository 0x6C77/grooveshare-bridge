'use strict';

class System {

    constructor(discovery) {
        this.discovery = discovery;
    }

    getStatus(player) {
        return player.state;
    }

    getRooms() {
        return this.discovery.zones.map((zone) => {
            return zone.coordinator.roomName;
        });
    }

}

module.exports = System;