'use strict';

class Spotify {
    constructor(player) {
        this.player = player;
    }

    play(spotifyUri, position = 0, queue = false) {
        const encodedSpotifyUri = encodeURIComponent(spotifyUri);
        const sid = this.player.system.getServiceId('Spotify');

        let uri;
        if (spotifyUri.startsWith('spotify:track:')) {
            uri = `x-sonos-spotify:${encodedSpotifyUri}?sid=${sid}&flags=32&sn=1`;
        } else {
            uri = `x-rincon-cpcontainer:0006206c${encodedSpotifyUri}`;
        }

        let metadata = this.getMetadata(encodedSpotifyUri);

        if (queue) {
            return this.player.coordinator.addURIToQueue(uri, metadata);
        }

        // Stop currently playing and play this song
        var nextTrackNo = this.player.coordinator.state.trackNo + 1;
        let promise = Promise.resolve();
        return promise.then(() => this.player.coordinator.setAVTransport(`x-rincon-queue:${this.player.coordinator.uuid}#0`))
            .then(() => this.player.coordinator.addURIToQueue(uri, metadata, true, nextTrackNo))
            .then((addToQueueStatus) => this.player.coordinator.trackSeek(addToQueueStatus.firsttracknumberenqueued))
            .then(() => {
                if (position) {
                    this.player.coordinator.timeSeek(position / 1000);
                }
            })
            .then(() => this.player.coordinator.play());
    }

    pause() {
        return this.player.coordinator.pause();
    }

    getMetadata(uri) {
        let serviceType = this.player.system.getServiceType('Spotify');

        return `<DIDL-Lite xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:upnp="urn:schemas-upnp-org:metadata-1-0/upnp/"
            xmlns:r="urn:schemas-rinconnetworks-com:metadata-1-0/" xmlns="urn:schemas-upnp-org:metadata-1-0/DIDL-Lite/">
            <item id="00030020${uri}" restricted="true"><upnp:class>object.item.audioItem.musicTrack</upnp:class>
            <desc id="cdudn" nameSpace="urn:schemas-rinconnetworks-com:metadata-1-0/">SA_RINCON${serviceType}_X_#Svc${serviceType}-0-Token</desc></item></DIDL-Lite>`;
    }
}

module.exports = Spotify;