Fork of the excellent [node-sonos-http-api](https://github.com/jishi/node-sonos-http-api) project. All praise and donations to the original author.

### Docker
```bash
docker build --tag grooveshare-bridge https://gitlab.com/0x6C77/grooveshare-bridge.git
docker run -d --net=host --restart unless-stopped --name grooveshare-bridge grooveshare-bridge

docker logs grooveshare-bridge
# You should see your token and a message saying the bridge connection was successful
```
