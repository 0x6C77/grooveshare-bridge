'use strict';
const fs = require('fs');
const path = require('path');
const crypto = require('crypto');

const nodeStatic = require('node-static');
const Pusher = require('pusher-js');

const SonosSystem = require('sonos-discovery');
const discovery = new SonosSystem();

const System = require('./lib/system');
const Spotify = require('./lib/spotify');



const winston = require('winston');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, printf } = format;

const logFormat = printf(({ level, message, timestamp }) => {
  return `[${timestamp}] ${level}: ${message}`;
});

const logger = winston.createLogger({
    level: 'info',
    format: combine(
        format.splat(),
        timestamp(),
        logFormat
    ),
    transports: [
        new transports.File({ filename: 'combined.log' }),
        new(transports.Console)({
            format: combine(
                format.colorize(),
                format.splat(),
                timestamp(),
                logFormat
            )
        })
    ],
});


// Connection to grooveshare server
const pusher = new Pusher('82a6310b306e87854369', {
    enabledTransports: ['ws'],
    wsHost: 'grooveshare.co.uk',
    wsPort: 6001,
    forceTLS: false
});

function readOrCreateToken() {
    const tokenFilePath = path.join(__dirname, 'token.txt');
    try {
        const token = fs.readFileSync(tokenFilePath).toString();
        return token;
    } catch (err) {
        if (err.code !== 'ENOENT') {
            throw err;
        }

        const token = crypto.randomBytes(16).toString('hex');
        fs.writeFileSync(tokenFilePath, token);
        return token;
    }
}


let secret = readOrCreateToken();

console.log("~~~~~~~~~~~~~");
console.log("Your secret is:", secret);
console.log("~~~~~~~~~~~~~");
console.log("");

// Generate secret key
const channel = pusher.subscribe('bridge-v1-' + secret);

channel.bind('pusher:subscription_succeeded', function(members) {
    logger.info('Connected to bridge channel');
});

channel.bind('getRooms', function (data) {
    let rooms = new System(discovery).getRooms();

    logger.info('Loaded rooms: %s', rooms);

    respond("getRooms", {
        "requestId": data.requestId,
        "rooms": new System(discovery).getRooms()
    });
});

channel.bind('getPlayerStatus', function (data) {
    let player = discovery.getPlayer(data.player);

    logger.info("Player status: %s", JSON.stringify(new System(discovery).getStatus(player)));

    respond("getPlayerStatus", {
        "requestId": data.requestId,
        "status": new System(discovery).getStatus(player)
    });
});

channel.bind('play', function (data) {
    let player = discovery.getPlayer(data.player);

    logger.info("Play: %s", JSON.stringify(data));

    let listening = true,
        reason = null;
    if (data.previous) {
        // Check we are in the same session
        let currentStatus = new System(discovery).getStatus(player);

        if (!currentStatus) {
            listening = false;
            reason = 'User has no session';
        } else {
            let statusPlaying = currentStatus['playbackState'] === 'PLAYING',
                correctSong = currentStatus['currentTrack']['uri'] === 'x-sonos-spotify:spotify%3atrack%3a' + data.previous + '?sid=9&flags=8224&sn=2';
            logger.info("Current status: %s %s %s %s", currentStatus['playbackState'], correctSong, currentStatus['elapsedTimeFormatted'], currentStatus['currentTrack']['uri']);

            if (currentStatus['playbackState'] === 'PAUSED_PLAYBACK') {
                listening = false;
                reason = 'User is paused';
            } else if (statusPlaying && !correctSong) {
                // User is listening to something different to the last song
                listening = false;
                reason = 'User is listening to something else';
            }
        }
    } else {
        // Clear queue
        player.coordinator.clearQueue();
    }

    // Tell server if we are still listening
    respond("play", {
        "requestId": data.requestId,
        "listening": listening,
        "reason": reason
    });

    if (listening) {
        (new Spotify(player)).play(data.song, data.position).then(() => {
            logger.info("Started playing: %s %s", data.song, data.position);
        });
    }
});

channel.bind('pause', function (data) {
    let player = discovery.getPlayer(data.player);
    (new Spotify(player)).pause().then(() => {
        logger.info("Paused");
    });
});


let respond = (event, data) => {
    logger.info("Response: %s %s", event, JSON.stringify(data));

    pusher.connection.connection.transport.socket.send(JSON.stringify({
        "event": event,
        "data": data
    }));
}
